import { Component } from 'react';
import Stepper from 'bs-stepper'
import { Form, Button} from 'react-bootstrap'
import { isEmpty } from "lodash";

// var DatePicker = require("react-bootstrap-date-picker");

import 'bs-stepper/dist/css/bs-stepper.min.css';
import "./styles.scss";

class Register extends Component {
    constructor() {
        super();
        this.state = {
            name: 'React',
            numberValue:'',
            codeValue:''
        };
    }

    componentDidMount() {
        this.stepper = new Stepper(document.querySelector('#stepper1'), {
            linear: false,
            animation: true
        })
    }

    onSubmit(e) {
        e.preventDefault()
    }

    onChangeNumberValue(event) {
        this.setState({
            numberValue:event.target.value,
             });
    }
    onChangeCodeValue(event) {
        this.setState({
            codeValue: event.target.value,
        });
    }
  
    shouldSubmitDisable = () => {
        return isEmpty(this.state.numberValue || isEmpty(this.state.codeValue) )  
      };

    render() {
        return (
            <div>
                <div id="stepper1" class="bs-stepper">
                    <div class="bs-stepper-header">
                        <div class="step" data-target="#test-l-1">
                            <button class="step-trigger">
                                <span class="bs-stepper-circle">۱</span>
                                <span class="bs-stepper-label">شماره همراه</span>
                            </button>
                        </div>
                        <div class="line"></div>
                        <div class="step" data-target="#test-l-2">
                            <button class="step-trigger">
                                <span class="bs-stepper-circle">۲</span>
                                <span class="bs-stepper-label">کد دریافتی</span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                        <form onSubmit={this.onSubmit}>
                            <div id="test-l-1" class="content">
                                <Form.Group className="mb-3" controlId="">
                                    <Form.Label>شماره همراه</Form.Label>
                                    <Form.Control onChange={this.onChangeNumberValue.bind(this)} type="" placeholder="" />
                                </Form.Group>
                            </div>

                            <div id="test-l-2" class="content">
                                <Form.Group className="mb-3" controlId="">
                                    <Form.Label>لطفا کد ارسالی را وارد نمایید</Form.Label>
                                    <Form.Control onChange={this.onChangeCodeValue.bind(this)}  className="otp" type="text" placeholder="" />
                                </Form.Group>
                            </div>

                            <div className="d-grid gap-2">
                                <Button disabled={this.shouldSubmitDisable()}   variant="primary" size="lg" onClick={() => this.stepper.next()}>
                                    دریافت رمز یکبار مصرف
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;